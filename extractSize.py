#!/usr/bin/python

### script for extracting size of contracts
import json
import sys
from os import path

class UnixTermColor():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    BOLD = '\33[1m'
    BLINK= '\33[5m'
    RESET = '\033[0m'


CRITICAL_SIZE = 24576
WARNING_SIZE = 1000
if len(sys.argv) < 2:
    sys.stderr.write("File to parse no spesified")
    exit(1)

sizeMap = {}

print('Processing...', end='', flush=True)

for fileName in sys.argv[1:]:
    try:
        f = open(fileName)
    except Exception as e:
        print(e, file=sys.stderr);
        continue
    
    
    try:
        abiFileContent = json.load(f)
    except Exception as e:
        print(e, fileName, "is it json file?",file=sys.stderr);
        continue

    f.close()
    sizeMap[path.basename(fileName)]= len(abiFileContent['bytecode'])/2 - 1

maxNameLen = max( len(i) for i in sizeMap)
formatString =  f'Size of %{maxNameLen}s = %s %6dB %7s' + UnixTermColor.RESET
print(end='\r')
for name, size in sizeMap.items():
    diff = 0
    if size == 0:
        color = UnixTermColor.CYAN
    elif size >= CRITICAL_SIZE:
        diff = CRITICAL_SIZE - size
        color = UnixTermColor.RED
    elif size >= CRITICAL_SIZE - WARNING_SIZE:
        color = UnixTermColor.YELLOW
        diff = CRITICAL_SIZE - size
    else:
        color = UnixTermColor.GREEN
    color = UnixTermColor.BOLD + color
    diff = str(int(diff))+'B' if diff else ''
    print(formatString %(name, color, size, diff))

