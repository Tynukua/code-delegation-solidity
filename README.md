# Code delegation example


## Current
```
Size of  Single.json =    2846B        
Size of Storage.json =      92B        
Size of    View.json =    1154B        
Size of  Worker.json =    1612B        
Size of Worker2.json =    2011B        
```

### start
```
Size of Single.json =    1893B        
Size of   View.json =     995B        
Size of Worker.json =    1612B    
```

### After replacing strings into HeavyCode 
```
Size of HeavyCode.json =    1306B        
Size of    Single.json =    1035B        
Size of      View.json =     995B        
Size of    Worker.json =     754B        
```

### After add new facet and deleting library

```
Size of  Single.json =    2793B        
Size of    View.json =    1923B        
Size of  Worker.json =    1612B        
Size of Worker2.json =    2011B   
```

### after creating common storage and using assambly
```
Size of  Single.json =    2846B        
Size of Storage.json =      92B        
Size of    View.json =    1154B        
Size of  Worker.json =    1612B        
Size of Worker2.json =    2011B        
```
