
const {assert} = require('chai');
const Worker = artifacts.require('Worker');
const Worker2 = artifacts.require('Worker2');
const View = artifacts.require('View');

contract('Delegate', async (accounts) =>{ 
  let worker;
  let worker2;
  let view;
  const SOMEBODY = accounts[1];
  const ANYBODY = accounts[2];  
  before('setup', async() =>{
    worker = await Worker.new();
    worker2 = await Worker2.new();
    view   = await View.new(worker.address, worker2.address); 
  });  
  it('should be delegated from somebody to anybody', async () =>{
    const AMOUNT = 100;  
    await view.delegate(ANYBODY, {from: SOMEBODY, value: AMOUNT});
    delegated = await view.getDelegateShare(SOMEBODY, ANYBODY);
    assert.equal(delegated.toString(), AMOUNT.toString())
  });  
  it('should be delegated from somebody by arrays', async () =>{
    const AMOUNT = 200;  
    const sum = await view.delegateArrays([ANYBODY, ANYBODY],[30, 70], {from: SOMEBODY});
    delegated = await view.getDelegateShare(SOMEBODY, ANYBODY);
    assert.equal(delegated.toString(), AMOUNT.toString())
    console.log(sum);  
    assert.equal(sum.toString(),'100');
  });  
});
